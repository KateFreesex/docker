#!/usr/bin/env bash

# install arch on proxmox with BTRFS
# With UEFI Boot mode need, when VM is booting need to go to the BIOS and disable secure boot mode, then save, then restart VM  

# Help when you can"t upgrade your system
# Error
# 
# :: Proceed with installation? [Y/n] Y
# error: Partition / too full: 142914 blocks needed, 63019 blocks free
# error: failed to commit transaction (not enough free disk space)
# Errors occurred, no packages were upgraded.
# 
# mount -o remount,size=3G /run/archiso/cowspace
# pacman -Syu --noconfirm && pacman -Syu --noconfirm git
# git clone https://gitlab.com/KateFreesex/notes.git
# chmod +x ./notes/arch_install.sh
# 

# echo "Delete all data on disk"
# dd if=/dev/zero of=/dev/sdb bs=4M status=progress;

###################### VARIABLES ######################


set -o errexit
set -o nounset
set -o pipefail

if [[ "${TRACE-0}" == "1" ]]; then
    set -o xtrace
fi

if [[ "${1-}" =~ ^-*h(elp)?$ ]]; then
    echo 'Usage: ./script.sh arg-one arg-two

This is an awesome bash script to make your life better.

'
    exit
fi

cd "$(dirname "$0")"

RED="\033[0;31m"
NC="\033[0m" # No Color
BLUE="\033[0;34m"
BLACK="\033[0;30m"
GREEN="\033[0;32m"
PURPUL="\033[0;35m"
YELLOW="\033[0;33m"
GRAY="\033[0;37m"
NORMAL="\e[0m"
NORMAL_FIND="\033[0m"


# Drive to install to.
DRIVE="/dev/sdb"

# mnt
MNT="/mnt"

# Hostname of the installed machine.
HOSTNAME="book"

# Encrypt everything (except /boot).  Leave blank to disable.
ENCRYPT_DRIVE="TRUE"

# Passphrase used to encrypt the drive (leave blank to be prompted).
DRIVE_PASSPHRASE="123"

# Root password (leave blank to be prompted).
ROOT_PASSWORD="123123"

# Main user to create (by default, added to wheel group, and others).
USER_NAME="user"

# The main user"s password (leave blank to be prompted).
USER_PASSWORD="123123"

# System timezone.
TIMEZONE="Europe/Amsterdam"

# Have /tmp on a tmpfs or not.  Leave blank to disable.
# Only leave this blank on systems with very little RAM.
TMP_ON_TMPFS="TRUE"

KEYMAP="us"
# KEYMAP="dvorak"

# Choose your video driver
# For Intel
VIDEO_DRIVER="i915"
# For nVidia
#VIDEO_DRIVER="nouveau"
# For ATI
#VIDEO_DRIVER="radeon"
# For generic stuff
#VIDEO_DRIVER="vesa"

# Wireless device, leave blank to not use wireless and use DHCP instead.
WIRELESS_DEVICE="wlan0"
# For tc4200"s
#WIRELESS_DEVICE="eth1"

###################### VARIABLES ######################


setup() {

    local drive="$DRIVE"
    local boot_dev="$DRIVE"1
    local swap_dev="$DRIVE"2
    local root_dev="$DRIVE"3

    echo -e -n "\n\n${RED}Checking...${NORMAL}\n\n"
    check
    echo -e -n "\n\n${RED}Checking is OK!${NORMAL}\n\n"

    echo -e "${GREEN}Creating partitions${NORMAL}\n\n"
    partition_drive "$drive"
    echo -e "${GREEN}Creating partitions - Done${NORMAL}\n\n"

    echo -e "${PURPUL}Formatting filesystems${NORMAL}\n\n"
    format_btrfs "$drive"
    echo -e "${PURPUL}Formatting filesystems - Done${NORMAL}\n\n"

    echo -e "${BLUE}Formatting filesystems${NORMAL}\n\n"
    format_boot_swap "$drive"
    echo -e "${BLUE}Formatting filesystems - Done${NORMAL}\n\n"

    echo -e "${YELLOW}Mount btrfs partitions${NORMAL}\n\n"
    mount_btrfs "$drive"
    echo -e "${YELLOW}Mount btrfs partitions - Done${NORMAL}\n\n"

    echo -e "${GRAY}Mount boot partitions${NORMAL}\n\n"
    mount_boot "$drive"
    echo -e "${GRAY}Mount boot partitions - Done${NORMAL}\n\n"

    echo -e "${BLUE}Update system${NORMAL}\n\n"
    update_os
    echo -e "${BLUE}Update system - Done${NORMAL}\n\n"

    echo -e "${YELLOW}Install packages${NORMAL}\n\n"
    install_base
    echo -e "${YELLOW}Install packages - Done${NORMAL}\n\n"

    echo -e "${PURPUL}Install genfstab${NORMAL}\n\n"
    genfstab_install
    echo -e "${PURPUL}Install genfstab - Done${NORMAL}\n\n"

    echo -e "${GRAY}Chroot config${NORMAL}\n\n"
    chroot_config_command
    echo -e "${GRAY}Chroot config - Done${NORMAL}\n\n"

    echo -e "${BLUE}FINISH!!!${NORMAL}\n\n"
}

check() {

    timedatectl set-ntp true;
    timedatectl set-timezone ${TIMEZONE}
    ls /sys/firmware/efi/efivars;
    timedatectl set-ntp true;
    timedatectl status;
    mount -o remount,size=3G /run/archiso/cowspace
}


partition_drive() {
    local dev="$1"; shift
    echo -e "\n\n${RED}$dev ${NORMAL}\n\n"
    command=$(parted -s "$dev" mklabel gpt mkpart P1 fat32 1MiB 1075MiB mkpart P2 linux-swap 1075MiB 7200MiB mkpart P3 btrfs 7200MiB 100% set 1 boot on)
    echo -e "\n\n${RED}${command} ${NORMAL}\n\n"
    # command
    fdisk -l;
    sleep 5
}

format_btrfs() {
    local dev="$1"; shift
    local root_dev="$dev"3
    echo -e ${dev}
    echo -e ${root_dev} 
    mkfs.btrfs -f ${root_dev};
    mount ${root_dev} /mnt;
    btrfs su cr /mnt/@;
    btrfs su cr /mnt/@home;
    btrfs su cr /mnt/@.snapshots;
    umount /mnt;
}


format_boot_swap() {
    local dev="$1"; shift

    local boot_dev="$dev"1
    local swap_dev="$dev"2
    echo -e ${dev}
    echo -e ${boot_dev}
    echo -e ${swap_dev}


    echo -e "\n\n${BLUE}${boot_dev} ${NORMAL}\n\n"
    mkfs.fat -F 32 -n EFI ${boot_dev};
    echo -e "\n\n${BLUE}${swap_dev} ${NORMAL}\n\n"
    mkswap ${swap_dev};
    swapon ${swap_dev};
    echo -e "\n\n${BLUE}$root_dev ${NORMAL}\n\n"
}

mount_btrfs() {
    local dev="$1"; shift
    local boot_dev="$dev"1
    local swap_dev="$dev"2
    local root_dev="$dev"3
    echo -e ${dev}
    echo -e ${boot_dev}
    echo -e ${swap_dev}
    echo -e ${root_dev} 
    echo "Mount btrfs partitions"
    # You need to manually create folder to mount the other subvolumes at
    mount -o noatime,commit=120,compress=zstd,subvol=@ ${root_dev} /mnt;
    mkdir /mnt/{boot,boot/efi,home,.snapshots};
    mount -o noatime,commit=120,compress=zstd,subvol=@home ${root_dev} /mnt/home;
    mount -o noatime,commit=120,compress=zstd,subvol=@.snapshots ${root_dev} /mnt/.snapshots;
}

mount_boot() {
    local dev="$1"; shift
    local boot_dev="$dev"1
    local swap_dev="$dev"2
    local root_dev="$dev"3
    echo "Mount boot partitions"
    # Mounting the boot partition at /boot folder
    mount ${boot_dev} /mnt/boot/efi;
}

update_os() {
    echo "Update system"
    echo "pacman -Syy --noconfirm"
    echo "pacman -S --noconfirm reflector"
    pacman -Syy --noconfirm;
    pacman -S --noconfirm reflector;

}


install_base() {
    echo "Install packages"

    echo "pacstrap ${MNT} base linux linux-firmware nano vim intel-ucode btrfs-progs"
    pacstrap ${MNT} base linux linux-firmware nano vim intel-ucode btrfs-progs
    # For Intel
    # pacstrap /mnt base linux linux-firmware nano intel-ucode btrfs-progs
    # For AMD
    # pacstrap /mnt base linux linux-firmware nano amd-ucode btrfs-progs
    # For VMs
    # pacstrap /mnt base linux linux-firmware nano btrfs-progs

}

genfstab_install() {
    echo "Install genfstab";

    echo "genfstab -U /mnt > /mnt/etc/fstab"
    genfstab -U /mnt > /mnt/etc/fstab;

}


chroot_config_command() {
    arch-chroot ${MNT} /bin/bash <<EOF
    
    locale-gen;
    echo en_US.UTF-8 UTF-8 > /etc/locale.conf;
    timedatectl set-timezone ${TIMEZONE}
    echo "127.0.0.1   localhost   ${HOSTNAME}" > /etc/hosts;
    echo "::1         localhost   ${HOSTNAME}" >> /etc/hosts;
    sed -i "s/MODULES=()/MODULES=(btrfs)/g" /etc/mkinitcpio.conf;
    echo "${HOSTNAME}" > /etc/hostname;
    useradd -m -G wheel -s /bin/bash user

    pacman -S --noconfirm dhcpcd iwd vi vim sudo networkmanager openssh;
    echo "AllowUsers user" >> /etc/ssh/ssh_config
    echo "PasswordAuthentication yes" >> /etc/ssh/ssh_config
    systemctl enable dhcpcd;
    systemctl enable sshd;

    pacman -S --noconfirm grub os-prober;
    grub-install ${DRIVE};
    pacman -S --noconfirm grub efibootmgr;
    mkdir /boot/efi;
    mount ${DRIVE}1 /boot/efi;
    grub-install --target=x86_64-efi --bootloader-id=GRUB --efi-directory=/boot/efi;
    grub-mkconfig -o /boot/grub/grub.cfg;

    echo 'root:${ROOT_PASSWORD}' | chpasswd
    echo 'user:${USER_PASSWORD}' | chpasswd
EOF
}

setup
